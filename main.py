from flask import Flask
from flask import request, abort
from waitress import serve
from schema import Schema, And, Use, Optional, SchemaError

from videoms.request import handle_request
from videoms.setups import setup_mongodb, load_config

config = load_config()
app = Flask(__name__)

def entry_schema():
    return Schema({
        'vodId': And(Use(int), lambda n: 1 <= len(str(n)) <= 10),
        'offset': And(Use(int), lambda n: 1 <= len(str(n)) <= 10),
        Optional('length'): And(Use(int), lambda n: 1 <= n <= 360), 
    })

@app.before_request
def limit_remote_addr():
    auth = request.headers.get('Authorization')
    if auth != config["authToken"]:
        abort(403)

@app.route(config["route"], methods=["POST"])
def entry():
    data = request.get_json()
    try:
        data = request_schema.validate(data)
    except SchemaError:
        return f'invalid body', 400

    cached_file = mongo_db.cached.find_one(data)
    if cached_file and 'filename' in cached_file:
        return f'{config["videoDomain"]}{cached_file["filename"]}.mp4', 200

    return handle_request(data, mongo_db, config)

if __name__ == "__main__":
    request_schema = entry_schema()
    mongo_db = setup_mongodb(config)

    serve(app, host=config["host"], port=config["port"], url_scheme='https')
