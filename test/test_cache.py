import pytest
import pathlib
import os

from videoms.request import handle_request
from videoms.setups import setup_mongodb, load_config

config = load_config()
mongo_db = setup_mongodb(config)

@pytest.mark.parametrize('vodId, offset, length, filename',
                         [('847944057', 0, 5, 'hvhULoB'),
                          ('847146232', 0, 5, 'gJM2G4f'),
                          ('847146232', 126000, 60, 'U2b1gtV'),
                          ('849125788', 171420, 60, 'JWwDtwh')])
def test_cache(vodId, offset, length, filename):
    data = {
        'vodId': vodId,
        'offset': offset,
        'length': length
    }

    cached_file = mongo_db['cached'].find_one(data)
    
    assert cached_file 
    assert 'filename' in cached_file
    assert cached_file['filename'] == filename
