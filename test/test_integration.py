import pytest
import pathlib
import os

from videoms.request import handle_request
from videoms.setups import setup_mongodb, load_config

config = load_config()
mongo_db = setup_mongodb(config)
video_domain = config['videoDomain']

@pytest.mark.parametrize('vodId, offset, length',
                         [('847944057', 0, 5),
                          ('847146232', 0, 5),
                          ('847146232', 126000, 60),
                          ('849125788', 171420, 60)])
def test_integration(vodId, offset, length):
    data = {
        'vodId': vodId,
        'offset': offset,
        'length': length
    }

    response = handle_request(data, config=config, mongo_db=mongo_db)

    response_code = response[1]
    assert response_code == 200

    response_link = response[0]
    assert video_domain in response_link

    returned_id = response_link[len(video_domain):len(video_domain)+7]

    mongo_doc = mongo_db.cached.find_one({'filename': returned_id})
    assert mongo_doc

    is_deleted = mongo_db.cached.delete_one({'filename': returned_id})
    assert is_deleted.acknowledged
    assert is_deleted.deleted_count

    os.remove(f'{config["videoPath"]}{returned_id}.mp4')
    assert len(list(pathlib.Path('.').glob('*.mp4'))) == 0

# mocker.patch(
#     'videoms.utils.id_exists',
#     return_value=False
# )

# mocker.patch(
#     'pymongo.collection.Collection.insert_one',
#     return_value=False
# )