import random
import string
import os

def random_id(length):
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))

def build_ffmpeg_command_list(offset, link, filename, length=60):
    return f'ffmpeg -y -loglevel quiet -protocol_whitelist https,file,tls,tcp,file,crypto -ss {offset} -i {link} -t {int(length)} -c copy {filename}.mp4'.split(' ')

def file_exists_locally(_id):
    return os.path.isfile(f'{_id}.mp4')

def filename_exists(collection, filename):
    return collection.find_one({'filename': filename})

def get_random_id(collection):
    filename = random_id(7)
    while filename_exists(collection, filename):
        filename = random_id(7)
    return filename
