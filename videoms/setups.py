import json

from pymongo import MongoClient


def load_config(path_to_config="config.json"):
    with open(path_to_config, "r") as read_file:
        return json.load(read_file)

def setup_mongodb(config=load_config()):
    client = MongoClient(f"mongodb://{config['mongodbUser']}:{config['mongodbPass']}@localhost:27017")
    db = client.clipsync
    db.authenticate(config["mongodbUserAccess"], config["mongodbPass"])
    return db