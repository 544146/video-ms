from re import findall

from urllib.parse import urlparse
from requests import Session
from .utils import random_id


s = Session()
s.headers.update({'client-id': 'kimne78kx3ncx6brgo4mv6wki5h1ko'})

def get_last_resort_m3u8(vod_id):
    gql_query = """
    query($id: ID) {
        video(id: $id) {
            animatedPreviewURL
        }
    }
    """
    query = {
        'query': gql_query,
        'variables': {'id': str(vod_id),},
    }

    response = s.post('https://gql.twitch.tv/gql', json=query)

    if response.ok:
        data = response.json()['data']
        try:
            animated_preview_url = data['video']['animatedPreviewURL']
            return transform_preview_url(animated_preview_url)
        except KeyError:
            pass

def transform_preview_url(animated_preview_url):
    try:
        a_url = urlparse(animated_preview_url)
        front = a_url.path.split("/")[1]
        return f'{a_url.scheme}://{a_url.netloc}/{front}/chunked/index-dvr.m3u8'
    except IndexError:
        pass

def get_m3u8(vod):
    token = s.get(f'https://api.twitch.tv/api/vods/{vod}/access_token?oauth_token=undefined&need_https=true&platform=_&player_type=site&player_backend=mediaplayer')

    if not token.ok:
        return get_last_resort_m3u8(vod)

    data = token.json()
    linkdata = s.get(f'https://usher.ttvnw.net/vod/{vod}.m3u8?player=twitchweb&token={data["token"]}&sig={data["sig"]}&type=any&allow_source=true&allow_audio_only=true&allow_spectre=false&p=1')

    if linkdata.ok:
        return findall(r'https:.*\.m3u8+|$', linkdata.text)[0]

    return get_last_resort_m3u8(vod)

def parse_m3u8(link):
    a_url = urlparse(link)
    front = a_url.path.split("/")[1]
    pre_link = f'{a_url.scheme}://{a_url.netloc}/{front}/chunked/'

    res = s.get(link)
    if not res.ok:
        raise Exception()

    lines = res.text.split('\n')
    chunks = []
    for x in range(0, len(lines)):
        if lines[x].startswith('#EXTINF:'):
            time_of_chunk = float(lines[x].split('#EXTINF:')[1].split(',')[0])
            chunks.append([time_of_chunk, pre_link + lines[x+1]])

    return chunks

def create_m3u8_with_offset(m3u8_info, offset):
    temp_offset = 0.0
    for x in range(0, len(m3u8_info)):
        temp_offset += m3u8_info[x][0]
        if (offset - temp_offset) < 20:
            links = ['#EXTM3U\n', '#EXT-X-VERSION:3\n', '#EXT-X-TARGETDURATION:10\n']
            for y in range(x, x+10 if (x+10 < len(m3u8_info)) else len(m3u8_info)):
                links += '#EXTINF:' + str(m3u8_info[y][0]) + ",\n"
                links += m3u8_info[y][1] + ('\n')

            links+= '#EXT-X-ENDLIST'
            file_name = f'{random_id(15)}.m3u8'
            with open(file_name, 'w+') as writer:
                writer.writelines(links)
            return file_name, (offset - (temp_offset - m3u8_info[x][0]))
        
    raise Exception()