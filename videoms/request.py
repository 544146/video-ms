import subprocess
import os

from .m3u8 import get_m3u8, parse_m3u8, create_m3u8_with_offset
from .constants import DEFAULT_CLIP_LENGTH, FFMPEG_WAIT_TIME
from .utils import build_ffmpeg_command_list, file_exists_locally, get_random_id

def handle_request(data, mongo_db=None, config=None):
    m3u8_link = get_m3u8(data["vodId"])
    if not m3u8_link:
        return 'failed to get m3u8', 400

    length = DEFAULT_CLIP_LENGTH
    if 'length' in data:
        length = data['length']

    offset = data['offset']
    filename = get_random_id(mongo_db.cached)

    m3u8_info = parse_m3u8(m3u8_link)
    input_file, offset = create_m3u8_with_offset(m3u8_info, offset)

    commands = build_ffmpeg_command_list(offset, input_file, filename, length)
    p = subprocess.Popen(commands)
    try:
        p.wait(FFMPEG_WAIT_TIME)
    except subprocess.TimeoutExpired:
        p.kill()
        if file_exists_locally(filename):
            os.remove(f'{filename}.mp4')
        return 'took too long to create mp4', 500

    if not file_exists_locally(filename):
        return 'failed to create mp4', 500

    os.rename(f'{os.getcwd()}/{filename}.mp4', f'{config["videoPath"]}{filename}.mp4')
    os.remove(input_file)

    data['filename'] = filename
    mongo_db.cached.insert_one(data)

    return f'{config["videoDomain"]}{filename}.mp4', 200
